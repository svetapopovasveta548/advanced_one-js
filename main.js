// 1) Прототипное наследование это когда объект может унаследовать значение , свойства и методы другого объекта.
// 2) super вызывает родительский конструктор.

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary
  }

  get name() {
    return this._name
  }

  set name(nameValue) {
    this._name = nameValue
  }

  get age() {
    return this._age
  }

  set age(ageValue) {
    this._age = ageValue
  }

  get salary() {
    return this._salary
  }

  set salary(salaryValue) {
    this._salary = salaryValue;
  }
}


class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang
  }
  get salary() {
    return this._salary * 3
  }
}
const programmerOne = new Programmer("Andrey", 23, 1500, "Java");
const programmerTwo = new Programmer("Masha", 21, 1900, "C++");
console.log(programmerOne);
console.log(programmerTwo);
